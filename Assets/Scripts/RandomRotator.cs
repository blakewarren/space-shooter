﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour
{
    public float tumble;

    private Rigidbody objectRigidbody;

    void Start()
    {
        objectRigidbody = GetComponent<Rigidbody>();

        objectRigidbody.angularVelocity = Random.insideUnitSphere * tumble;
    }

}
