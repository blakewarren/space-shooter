﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public float speed;
    private Rigidbody boltRigidbody;
    
    void Start ()
    {
        boltRigidbody = GetComponent<Rigidbody>();

        boltRigidbody.velocity = transform.forward * speed;
    }
}
